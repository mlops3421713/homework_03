from homework.eda import load, generate_report

df = load("data/2015-street-tree-census-tree-data.csv")

generate_report(df, "data/report.html")
