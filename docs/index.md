# Exploring NY 2015 Street Tree Census Data

The dataset includes street tree data from the TreesCount! 2015 Street Tree Census, conducted by volunteers and staff organized by NYC Parks & Recreation and partner organizations. Tree data collected includes the information such as tree species, diameter and perception of health.

See the **automated EDA report** for the basic data overview (data types, distributions, missing values and other common issues).

See the **detailed EDA report** for the feature dependecies and mapboxes.

The following preprocessing is applied:

- Using `tree_id` as index: note this creates duplicated lines, which may be a subject for removal when training an ML model, but are kept for the exploration part since they have their unique `tree_id`.
- Sorting by the `tree_id`.
- Parsing `created_at` as datatime.
- Dropping `state` column, which is currently constant and refers to New York City.
