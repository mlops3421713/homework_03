# MLops homework: CI/CD

This is a homework repository for the [MLOps & production for data science research 3.0](https://ods.ai/tracks/mlops3-course-spring-2024).

This one is an automated Gitlab pages deployment pipeline. You can see the deployed research at https://homework-03-mlops3421713-a1d6b7aa93dfbcc608c722bb03a77a57f2c004.gitlab.io/
