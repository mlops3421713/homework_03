"""
This module contains common EDA routines.
"""

import pandas as pd
import plotly.express as px

from ydata_profiling import ProfileReport


# A colormap for health tree display.
health_color = {
    "Good": "green",
    "Fair": "orange",
    "Poor": "red",
    "Dead or stump": "black",
}

# Problem decryption.
problem_dict = {
    "root_stone": "Root problems caused by paving stones",
    "root_grate": "Root problem caused by metal grates",
    "root_other": "Other root problems",
    "trunk_wire": "Trunk problems caused by wires or ropes",
    "trnk_light": "Trunk problem caused by lighting installed",
    "trnk_other": "Other trunk problems",
    "brch_light": "Branch problems caused by lights or wires",
    "brch_shoe": "Branch problem caused by sneakers in th|e branches",
    "brch_other": "Other branch problems",
}


def load(path):
    """
    Loads a dataset with minimal preprocessing.

    Args:
        path (str): path to a CSV file

    Returns:
        pd.DataFrame: a loaded dataset
    """
    return (
        pd.read_csv(
            path,
            parse_dates=["created_at"],
            index_col="tree_id",
        )
        .drop(["state"], axis=1)
        .sort_index()
    )


def generate_report(df, output_file):
    """
    Generates an ydata-profiling automated report.

    Args:
        df (pd.DataFrame): a dataframe to proceed
        output_file (str): a file name for the report to be saved to
    """
    report = ProfileReport(df, title="Profiling Report")
    report.to_file(output_file)


def mapbox(df, cb_start, cb_end):
    """
    Displays an interactive map, limited by community board range.
    The first digit of a community board matches the borough code.

    Args:
        df (pd.DataFrame): a dataframe to proceed
        cb_start (int): community board index to start
        cb_end (int): community board index to finish

    Returns:
        plotly.graph_objs._figure.Figure: the map object
    """
    return px.scatter_mapbox(
        df[
            (df["community board"] >= cb_start)
            & (df["community board"] <= cb_end)
        ],
        lat="latitude",
        lon="longitude",
        # hover_name="health",
        hover_data=["spc_common"],
        color="health",
        color_discrete_map=health_color,
        zoom=11,
        height=600,
        width=600,
        mapbox_style="carto-positron",
    )
